## Building and running

The Dockerfile allows building an image for PBC testnet or mainnet. 
To build the testnet image, pass `--build-arg TESTNET=1` to `docker build`. The default behaviour is to build the mainnet image.

Running the image can be done by following the instructions [here](https://partisiablockchain.gitlab.io/documentation/operator-2-reader.html).

For example, to run a testnet node with a minimum config, where the rosetta API can be queried at `http://localhost:8080/rosetta`:

```
mkdir storage
docker buid --build-arg TESTNET=1 -t rosetta .
docker run -i -p 8080:8080 -v $(pwd)/conf.conf:/conf.conf -v $(pwd)/storage/storage:/storage -t rosetta conf.conf storage
```

where `conf.conf` is a file with the following content

```
{
  "restPort": 8080,
  "floodingPort": 9888,
  "knownPeers": []
}
```
